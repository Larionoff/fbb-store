'use strict';

var
  queryParams = function()
  {
    var urlParts = location.href.split('?');
    if (urlParts.length!=2) return [];
    var
      params = urlParts[1].split('&'),
      result = {};
    params.forEach(function(item)
    {
      var keyValue = item.split('=');
      if (keyValue.length==2) result[keyValue[0].toLowerCase()] = keyValue[1];
    });
    return result;
  }();

function scopeOf(controller)
{
  return angular.element(document.querySelector('[ng-controller="'+controller+'"]')).scope();
}

function later(minutes)
{
  return new Date(new Date().valueOf()+minutes*60000).toUTCString();
}

/* -------------------- APPLICATION -------------------- */

var bookApp = angular.module('BookApp',['ngCookies']).run(function($rootScope,currentBook,orderBook)
{
  $rootScope.onSearchInput = function()
  {
    currentBook.setId(null);
    orderBook.setId(null);
  };
  $rootScope.onChangeCurrency = function()
  {
    scopeOf('CurrencyCtrl').shown = true;
  };
});

/* ---------------------- FILTERS ---------------------- */

bookApp.filter('html',function($sce)
{
  return $sce.trustAsHtml;
});

/* --------------------- SERVICES ---------------------- */

bookApp.service('currentBook',function($rootScope)
{
  this.id = queryParams.id===undefined ? null : queryParams.id;
  $rootScope.logoClickable = this.id==null ? '' : 'clickable';
  this.setId = function(newId)
  {
    this.id = newId;
    scopeOf('BookListCtrl').setId(newId);
    scopeOf('BookCtrl').setId(newId);
    if (newId==null)
    {
      history.replaceState(null,null,'index.html');
      $rootScope.logoClickable = '';
    }
  };
});

bookApp.service('orderBook',function($rootScope,currentBook)
{
  this.id = queryParams.order===undefined ? null : queryParams.order;
  $rootScope.logoClickable = this.id==null && currentBook.id==null ? '' : 'clickable';
  this.setId = function(newId)
  {
    this.id = newId;
    scopeOf('OrderCtrl').setId(newId);
    if (newId==null)
    {
      history.replaceState(null,null,'index.html');
      $rootScope.logoClickable = '';
    }
  }
});

bookApp.service('currency',function($cookies)
{
  var data = $cookies.get('currency');
  if (data!==undefined) data = JSON.parse(data);
  this.initialized = data!==undefined;
  if (this.initialized)
  {
    this.id = data.id;
    this.code = data.code;
    this.value = data.value;
    this.nominal = data.nominal;
  } else {
    this.id = 'R01589';
    this.code = 'ZZZ';
    this.value = 80.84;
    this.nominal = 1;
  }
  this.setValues = function(id,code,value,nominal)
  {
    this.id = id;
    this.code = code;
    this.value = value;
    this.nominal = nominal;
    $cookies.put('currency',JSON.stringify({id: this.id, code: this.code, value: this.value, nominal: this.nominal}),{expires: later(10080)});
    this.initialized = true;
  };
  this.convert = function(price)
  {
    var code;
    switch (this.code)
    {
      case 'EUR': code = '&euro;'; break;
      case 'UAH': code = '&#8372;'; break;
      case 'USD': code = '$'; break;
      case 'ZZZ': code = '<span class="s">Z</span>'; break;
      default: code = this.code; break;
    }
    return (price / this.value * this.nominal).toFixed(2) + '&nbsp;' + code;
  };
});

/* -------------------- CONTROLLERS -------------------- */

bookApp.controller('BookListCtrl',function($scope,$http,$rootScope,$cookies,currentBook,orderBook)
{
  $scope.id = currentBook.id;
  $scope.visible = $scope.id==null && orderBook.id==null;
  $scope.books = [];
  $scope.shown = $cookies.get('booksShown');
  if ($scope.shown===undefined)
  {
    $scope.shown = 4;
    $cookies.put('booksShown',$scope.shown,{expires: later(5)});
  }
  $scope.showMore = function()
  {
    $scope.shown += 20;
    $cookies.put('booksShown',$scope.shown);
  };
  $scope.buttonVisible = function()
  {
    return $scope.shown<$scope.books.length && ($rootScope.search===undefined || $rootScope.search=='');
  };
  $http({
    method: 'GET',
    url: 'https://netology-fbb-store-api.herokuapp.com/book'
  }).then(function(response)
  {
    $scope.books = response.data;
  });
  $scope.setId = function(id)
  {
    $scope.id = id;
    $scope.visible = $scope.id==null && orderBook.id==null;
  };
});

bookApp.controller('BookCtrl',function($scope,$http,$cookies,currentBook,orderBook,currency)
{
  $scope.id = currentBook.id;
  $scope.visible = $scope.id!=null && orderBook.id==null;
  $scope.setId = function(id)
  {
    $scope.id = id;
    $scope.visible = $scope.id!=null && orderBook.id==null;
  };
  if (!$scope.visible) return;
  $scope.book = null;
  $http({
    method: 'GET',
    url: 'https://netology-fbb-store-api.herokuapp.com/book/' + $scope.id
  }).then(function(response)
  {
    $scope.book = response.data;
    $scope.book.priceText = currency.convert($scope.book.price);
  });
});

bookApp.controller('OrderCtrl',function($scope,$http,orderBook,currency)
{
  $scope.re = {
    EMAIL: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    PHONE_NUMBER: /^( +)?((\+?7|8) ?)?((\(\d{3}\))|(\d{3}))?( )?(\d{3}[\- ]?\d{2}[\- ]?\d{2})( +)?$/,
    USERNAME: /^[a-zA-Zа-яА-Я- ]{3,30}$/
  };
  $scope.order = orderBook.id;
  $scope.visible = $scope.order!=null;
  if (!$scope.visible) return;
  $scope.book = null;
  $scope.deliveries = [];
  $scope.payments = [];
  $scope.data = {delivery: {}, payment: {}};
  $http({
    method: 'GET',
    url: 'https://netology-fbb-store-api.herokuapp.com/book/' + $scope.order
  }).then(function(response)
  {
    $scope.book = response.data;
  });
  $http({
    method: 'GET',
    url: 'https://netology-fbb-store-api.herokuapp.com/order/delivery'
  }).then(function(response)
  {
    $scope.deliveries = response.data;
    $scope.data.delivery.id = $scope.deliveries[0].id;
  });
  $http({
    method: 'GET',
    url: 'https://netology-fbb-store-api.herokuapp.com/order/payment'
  }).then(function(response)
  {
    $scope.payments = response.data;
    $scope.data.payment.id = $scope.payments[0].id;
  });
  $scope.getPriceAsText = function(p)
  {
    return p==0 ? 'бесплатно' : currency.convert(p);
  };
  $scope.setId = function(id)
  {
    $scope.order = id;
    $scope.visible = $scope.order!=null;
  };
  $scope.orderStatus = '';
  $scope.loading = false;
  $scope.success = false;
  $scope.orderResult = function(response)
  {
    var data = response.data;
    if (data.status=='success')
    {
      $scope.success = true;
    } else {
      alert('ERROR!');
    }
  };
  $scope.makeOrder = function()
  {
    $scope.data.manager = 'larionov@programmer.net';
    $scope.data.book = $scope.book.id;
    $scope.data.payment.currency = currency.id;
    $scope.loading = true;
    $http({
      method: 'POST',
      url: 'https://netology-fbb-store-api.herokuapp.com/order',
      data: $scope.data
    }).then($scope.orderResult,$scope.orderResult);
  };
  $scope.paymentAllowed = function(index)
  {
    var deliveryId = $scope.data.delivery==undefined ? $scope.deliveries[0].id : $scope.data.delivery.id;
    return $scope.payments[index].availableFor.indexOf(deliveryId)>=0;
  };
  $scope.addressNeeded = function()
  {
    if ($scope.data.delivery==undefined) return false;
    for (var i=0; i<$scope.deliveries.length; i++)
    {
      if ($scope.data.delivery.id==$scope.deliveries[i].id) return $scope.deliveries[i].needAdress;
    }
    return false;
  };
  $scope.selectCorrectPaymentMethod = function()
  {
    var paymentIndex = -1, i;
    for (i=0; i<$scope.payments.length; i++)
    {
      if ($scope.payments[i].id==$scope.data.payment.id)
      {
        paymentIndex = i;
        break;
      }
    }
    if (paymentIndex<0) return;
    if ($scope.payments[paymentIndex].availableFor.indexOf($scope.data.delivery.id)<0)
    {
      for (i=0; i<$scope.payments.length; i++)
      {
        if (i==paymentIndex) continue;
        if ($scope.payments[i].availableFor.indexOf($scope.data.delivery.id)>=0)
        {
          $scope.data.payment.id = $scope.payments[i].id;
          break;
        }
      }
    }
  };
  $scope.deliveryCost = 0;
  $scope.calculateDeliveryCost = function(index)
  {
    $scope.deliveryCost = $scope.deliveries[index].price;
    $scope.selectCorrectPaymentMethod();
  };
  $scope.showError = false;
  $scope.hasInvalidFields = function()
  {
    return document.querySelectorAll('form[name="orderForm"] .ng-invalid:not(.ng-untouched)').length>0;
  };
  $scope.onTextInputChange = function()
  {
    $scope.showError = $scope.hasInvalidFields();
  };
});

bookApp.controller('CurrencyCtrl',function($scope,$http,$cookies,currency)
{
  $scope.shown = false;
  $scope.value = currency.id;
  $scope.currencies = [];
  $http({
    method: 'GET',
    url: 'https://netology-fbb-store-api.herokuapp.com/currency'
  }).then(function(response)
  {
    $scope.currencies = response.data;
    $scope.shown = !currency.initialized;
  });
  $scope.closePopup = function()
  {
    $scope.shown = false;
  };
  $scope.setNewCurrency = function()
  {
    var selectedCurrency = null;
    for (var i=0; i<$scope.currencies.length; i++)
    {
      if ($scope.value==$scope.currencies[i].ID)
      {
        selectedCurrency = $scope.currencies[i];
        break;
      }
    }
    if (selectedCurrency) currency.setValues($scope.value,selectedCurrency.CharCode,selectedCurrency.Value,selectedCurrency.Nominal);
    var bookCtrlScope = scopeOf('BookCtrl');
    if (bookCtrlScope.book!==undefined) bookCtrlScope.book.priceText = currency.convert(bookCtrlScope.book.price);
    $scope.shown = false;
  };
});

/* ----------------------------------------------------- */