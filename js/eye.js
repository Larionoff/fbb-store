var Eye = {
  section: document.getElementsByClassName('book-details')[0],
  container: document.getElementsByClassName('eye')[0],
  bulb: document.getElementById('bulb'),
  lid: document.getElementById('lid'),
  pupil: document.getElementById('pupil'),
  scaleFactor: 1.0,
  animationEnd: function()
  {
    var delay = Math.floor(Math.random()*8)+3; // 3..10
    Eye.lid.style.animationDelay = delay + 's';
    Eye.restartAnimation();
  },
  getCenter: function()
  {
    var r = Eye.bulb.getBoundingClientRect();
    return new Point(r.left+(r.right-r.left)*0.5,r.top+(r.bottom-r.top)*0.5);
  },
  getMaxTranslation: function()
  {
    return (Eye.lid.clientWidth - Eye.pupil.clientWidth) * 0.5;
  },
  initialize: function()
  {
    this.resize();
    document.body.addEventListener('mousemove',this.mouseMove);
    window.addEventListener('resize',this.resize);
    this.lid.addEventListener('animationend',this.animationEnd);
    this.lid.addEventListener('webkitAnimationEnd',this.animationEnd);
  },
  mouseMove: function(event)
  {
    if (Eye.section.classList.contains('ng-hide')) return;
    var
      mouse = new Point(event.clientX,event.clientY),
      eye = Eye.getCenter(),
      dir = eye.directionTo(mouse),
      dist = eye.distanceTo(mouse) * Eye.scaleFactor,
      max = Eye.getMaxTranslation();
    if (dist>max) dist = max;
    Eye.pupil.style.transform = 'rotate(' + dir + 'rad) translateX(' + dist + 'px)';
  },
  resize: function()
  {
    Eye.scaleFactor = (Eye.bulb.clientWidth - Eye.pupil.clientWidth) / Math.max(window.innerWidth,window.innerHeight);
  },
  restartAnimation: function()
  {
    Eye.lid.classList.remove('blink');
    setTimeout(function()
    {
      Eye.lid.classList.add('blink');
    },1);
  }
};

Eye.initialize();