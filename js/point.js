function Point(x,y)
{
  if (arguments.length<2)
  {
    x = 0;
    y = 0;
  }
  this.x = x;
  this.y = y;
}

Point.prototype.directionTo = function(p)
{
  var a = Math.asin((p.y-this.y)/this.distanceTo(p));
  return p.x-this.x>=0 ? a : Math.PI-a;
};

Point.prototype.distanceTo = function(p)
{
  function sqr(x){return x*x;}
  return Math.sqrt(sqr(this.x-p.x)+sqr(this.y-p.y));
};

Point.prototype.set = function(x,y)
{
  this.x = x;
  this.y = y;
};

Point.prototype.css = function()
{
  return 'left:' + this.x + 'px;top:' + this.y + 'px';
};

Point.prototype.toString = function()
{
  return '[' + this.x + ',' + this.y + ']';
};

Point.prototype.add = function(p)
{
  return new Point(this.x+p.x,this.y+p.y);
};

Point.prototype.subtract = function(p)
{
  return new Point(this.x-p.x,this.y-p.y);
};