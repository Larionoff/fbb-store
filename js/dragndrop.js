var DragDrop = {
  draggable: null,
  dropTargets: [],
  element: null,
  elements: [],
  initialPosition: new Point(),
  mousePosition: new Point(),
  initializeDropTargets: function()
  {
    this.dropTargets = [];
    var scroll = new Point(window.scrollX,window.scrollY);
    this.elements.forEach(function(item,i)
    {
      var
        r = item.getBoundingClientRect(),
        cx = (r.right - r.left) * 0.5 + r.left,
        cy = (r.bottom - r.top) * 0.5 + r.top;
      // DragDrop.dropTargets.push({center: new Point(cx,cy).add(scroll), left: new Point(r.left,cy).add(scroll)});
      DragDrop.dropTargets.push(new Point(cx,cy).add(scroll));
    });
  },
  initialize: function()
  {
    var elems = document.getElementsByClassName('book');
    for (var i=0; i<elems.length; i++) this.elements.push(elems[i]);
    this.elements.forEach(function(item,i)
    {
      item.setAttribute('data-index',i);
      item.addEventListener('click',DragDrop.onClick);
      item.addEventListener('mousedown',DragDrop.onMouseDown);
    });
    this.initializeDropTargets();
    document.body.addEventListener('mousemove',DragDrop.onMouseMove);
    document.body.addEventListener('mouseup',DragDrop.onMouseUp);
  },
  getDropTarget: function()
  {
    var elem = document.getElementsByClassName('drop-target');
    return elem.length>0 ? elem[0] : null;
  },
  getNearestTarget: function(p)
  {
    var min = 2147483647, index = -1;
    DragDrop.dropTargets.forEach(function(item,i)
    {
      var d = item.distanceTo(p);
      if (d<min)
      {
        min = d;
        index = i;
      }
    });
    return index;
  },
  getRoot: function(element)
  {
    var root = element;
    while (!root.classList.contains('book'))
    {
      root = root.parentNode;
    }
    return root;
  },
  onClick: function(event)
  {
    if (DragDrop.element)
    {
      if (DragDrop.element.hasAttribute('prevent-default'))
      {
        DragDrop.element.removeAttribute('prevent-default');
        event.preventDefault();
      }
      DragDrop.element = null;
    }
  },
  onMouseDown: function(event)
  {
    if (event.button!=0) return;
    if (event.target.classList.contains('book__cover'))
    {
      DragDrop.element = DragDrop.getRoot(event.target);
      DragDrop.element.setAttribute('prevent-default','');
      DragDrop.draggable = DragDrop.element.cloneNode(true);
      DragDrop.draggable.classList.add('draggable');
      var r = DragDrop.element.getBoundingClientRect();
      DragDrop.initialPosition.set(r.left+(DragDrop.element.clientWidth-280)*0.5+window.scrollX,r.top+window.scrollY);
      DragDrop.draggable.style = DragDrop.initialPosition.css();
      document.body.appendChild(DragDrop.draggable);
      DragDrop.mousePosition.set(event.pageX,event.pageY);
      DragDrop.element.classList.add('hidden');
      DragDrop.onMouseMove(event);
      event.preventDefault();
    }
  },
  onMouseMove: function(event)
  {
    if (DragDrop.draggable)
    {
      var mouse = new Point(event.pageX,event.pageY);
      var offset = mouse.subtract(DragDrop.mousePosition);
      DragDrop.draggable.style = DragDrop.initialPosition.add(offset).css();
      var nearest = DragDrop.getNearestTarget(mouse);
      DragDrop.elements.forEach(function(item,i)
      {
        var method = i==nearest ? item.classList.add : item.classList.remove;
        method.call(item.classList,'drop-target');
      });
    }
  },
  onMouseUp: function()
  {
    var target = DragDrop.getDropTarget();
    if (target)
    {

      if (target.getAttribute('data-index')==DragDrop.element.getAttribute('data-index'))
      {
        DragDrop.element.removeAttribute('prevent-default');
        location.href = DragDrop.element.getElementsByClassName('book__link')[0].href;
      } else {
        target.parentNode.insertBefore(DragDrop.element,target);
        setTimeout(function()
        {
          DragDrop.initializeDropTargets();
        },10);
      }
    }
    DragDrop.draggable.parentNode.removeChild(DragDrop.draggable);
    DragDrop.draggable = null;
    DragDrop.element.classList.remove('hidden');
    DragDrop.elements.forEach(function(item)
    {
      item.classList.remove('drop-target');
    });
  }
};

document.body.onload = function()
{
  setTimeout(function()
  {
    if (document.getElementsByClassName('books')[0].classList.contains('ng-hide')) return;
    DragDrop.initialize();
    document.addEventListener('scroll',function()
    {
      DragDrop.initializeDropTargets();
    });
  },1000);
};