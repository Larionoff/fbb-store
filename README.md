# Книжный магазин издательства «Фрай, без штанов и Бендер» #

[Рабочая версия](http://university.netology.ru/user_data/larionov/fbb-store)

Страницы сайта:

* список книг;
* страница книги;
* о компании;
* оформление заказа.

## Список книг ##

На странице «Список книг» по умолчанию отображены 4 книги. При нажатии на кнопку «Ещё несколько книг» на страницу будут добавляться ещё 4 книги до тех пор, пока не будут показаны все имеющиеся книги. Когда все книги будут отображены, кнопка «Ещё несколько книг» будет скрыта. Книги в списке можно менять местами, перетаскивая их с помощью мыши. Щелчок по книге в списке откроет страницу с её подробным описанием. Щелчок по логотипу с Бендером внизу страницы откроет страницу «О компании». При первом посещении сайта будет отображено всплывающее окно, в котором необходимо выбрать валюту, которая будет использоваться при оформлении заказа.

## Страница книги ##

На этой странице отображается подробная информация о книге и предоставляется возможность её заказать. При щелчке по логотипу сайта будет произведён переход на главную страницу. При нажатии кнопки «Купить» будет произведён переход на страницу оформления заказа. На обложке каждой книги размещён глаз, который наблюдает за курсором мыши и моргает через случайные промежутки времени от 3 до 10 секунд.

## О компании ##

Страница содержит текст повествующий о компании. Логотип с Бендером в нижней части страницы не кликабельный. При щелчке по логотипу сайта производится переход на главную страницу.

## Форма оформления заказа ##

На странице оформления заказа отображено название выбранной пользователем книги, её обложка и форма со следующими полями:

* имя;
* телефон;
* электронный адрес;
* комментарий к заказу;
* способ доставки;
* способ оплаты;
* адрес доставки (поле отображается только если для выбранного способа доставки необходим адрес).

Далее показана итоговая стоимость заказа, равная сумме стоимости книги и стоимости её доставки выбранным способом. Кнопка «Оформить заказ» отправляет данные о заказе на сервер. Кнопка не доступна, если поля формы заполнены с ошибкой, при этом ошибочные поля подсвечиваются красным цветом. Проверка правильности введённых данных производится по мере набора текста, при этом пустые нетронутые поля не считаются ошибочными. Пункты в списке способов оплаты также могут быть недоступны, если способ оплаты недоступен при выбранном способе доставки.

## Поиск ##

Поиск производится по названиям и описаниям книг и работает в виде фильтра. Книги, названия или описания которых не содержат введённую в поле поиска фразу, будут скрываться со страницы по мере набора текста. При очистке поля поиска будут отображены все присутствующие ранее на странице книги. Если поиск был начал не с главной страницы, то будет произведён переход на главную страницу к списку книг.

# Технические детали #

## Запуск проекта ##

* создать копию репозитория на компьютере
```
git clone https://Larionoff@bitbucket.org/Larionoff/fbb-store.git
```
* установить зависимости
```
npm install
```
* запустить локальный сервер
```
npm start
```

## Зависимости ##

* **angular** – для разработки одностраничного приложения;
* **angular-cookies** – для работы с куками.